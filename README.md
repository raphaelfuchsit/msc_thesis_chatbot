# Introduction
This repository was developed as part of the master thesis "GPT-based chatbot in the context of medical knowledge transfer" at the Zurich University of Applied Sciences. It contains various implementations, a knowledge base, and a prototype chatbot based on the GPT-2 Transformer.

* Author: Raphael Fuchs
* First reviewer: Prof. Dr. Thomas Keller
* Second reviewer: Prof. Dr. Alexandre de Spindler

Frauenfeld, 21.05.2021

# Installation & Startup

## Startup with virtual python environment
All informations under following link:
https://docs.python.org/3/tutorial/venv.html

Create/delete a new virtual environment:
```
python -m venv ...
python -m venv --clear .env
```
Start/end virtual environment. Important to start env. otherwise changes to environments goes to another one:
```bat
# Start
.\venv\Scripts\activate

# End
deactivate
```
### Install packages
```
py -m pip install ...
```
### Save actual state of env in requirements.txt and install again
```
# Save
pip freeze > requirements.txt

# Install
pip install -r requirements.txt
```
