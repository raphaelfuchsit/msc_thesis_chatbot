Negativer Kontextwechsel: Antwort startet mit kontextuellem Bezug zum Thema, jedoch wechselt der Kontext mind. nach dem ersten Satz sodass, die Aussage nicht mehr zum Thema passt oder keinen Sinn ergibt.
Positiver Kontextwechsel: Satz beginnt wie antrainert, jedoch weicht der Inhalt von der antrainierten Antwort ab, so dass der Inhalt grundsätzlich noch zum Thema passt.
Falscher Kontext: Antwort beginnt nicht wie antrainiert und hat inhaltlich keinen Bezug zum entsprechnenden Themas
Kontexterweiterung: Der entsprechende Inhalt zum angefragten Thema wird zwar wiedergegeben, jedoch mit fremnden kontextuellen Elementen erweitert
Unvollständiger Inhalt: Das wiedergegebene ist korrekt, jedoch fehlen antrainierte Inhalte
Identisch: Der Output entspricht exakt dem antrainierten Text

Korrekte Ansprache: Formell / Informell (kann das erkennt werden?)
Korrekte Grammatik: Existieren grammatikalische Fehler, falsche Wörter, Wörter die nicht existieren, etc..

Erkentnissse
- "Therapie, Peritonealdialyse, Häufigkeit" zieht Kontext von "Therapie, Hämodialye, Häufigkeit"
- "Nierenerkrankung, Symptome, Selbstdiagnose" --> "Nierenerkrankung, Reversibilität"
- Ähnlicher Kontext herbegezogen: Nierenerkrankung, Symptome, Selbstdiagnose --> Nierenerkrankung, Diagnose