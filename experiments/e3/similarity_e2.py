import pandas
import spacy
import re
from datetime import datetime
from collections import Counter
from scipy.spatial.distance import cosine

nlp = spacy.load("de_core_news_lg")
output_path = '/experiments/e3/'
pd_base_data_answers = pandas.read_excel(output_path+'Wissenskorpus_E2.xlsx', sheet_name='Antworten')
pd_generation_data = pandas.read_excel(output_path+'output_merged_2.xlsx', sheet_name='Tabelle1')
evaluation_filename = 'output_merged_2'
evaluation_columns = [
    'AnswerType',
    'ID',
    'TryNbr',
    'Type',
    'StartDate',
    'ConsumedSeconds',
    'GeneratedKey',
    'GeneratedKeyLen',
    'SourceAnswer',
    'GeneratedAnswer',
    'SourceAnswerLen',
    'GeneratedAnswerLen',
    'LenDiff',
    'AnswerCosineSimilarityEmbedding',
    'AnswerCosineSimilarity'
]

# def get_cosine_sim(*strs):
#     vectors = [t for t in get_vectors(*strs)]
#     return cosine_similarity(vectors)
#
# def get_vectors(*strs):
#     text = [t for t in strs]
#     vectorizer = CountVectorizer(text)
#     vectorizer.fit(text)
#     return vectorizer.transform(text).toarray()

output = []


def cosine_distance_countvectorizer_method(s1, s2):
    # sentences to list
    sentences = [s1 , s2]

    # packages
    from sklearn.feature_extraction.text import CountVectorizer
    from scipy.spatial import distance
    from sklearn.metrics.pairwise import cosine_similarity

    # text to vector
    vectorizer = CountVectorizer()
    all_sentences_to_vector = vectorizer.fit_transform(sentences)
    text_to_vector_v1 = all_sentences_to_vector.toarray()[0].tolist()
    text_to_vector_v2 = all_sentences_to_vector.toarray()[1].tolist()

    # distance of similarity
    cosine = distance.cosine(text_to_vector_v1, text_to_vector_v2)
    return 1-cosine



def calc_cosine_similarity_embedding(s1, s2):
    sentences = [s1, s2]
    vectors = [nlp(sentence).vector for sentence in sentences]
    cosine_similarity = cosine(vectors[0], vectors[1])
    return 1-cosine_similarity


def nlp_processor(text):
    doc = nlp(re.sub('\n', '', text))
    return ' '.join([word.lemma_.lower() for word in doc if not word.is_stop and not word.is_punct])


for index, series in pd_generation_data.iterrows():
    id = series['ID']
    generated_key = series['Key']
    generated_answer = series['Answer']
    answer_type = series['Type']

    # Calculate generation length
    generated_key_len = len(nlp(generated_key)) if not pandas.isnull(generated_key) else 0
    generated_answer_len = len(nlp(generated_answer)) if not pandas.isnull(generated_answer) else 0

    # Find entries from finetune sheet
    subset = pd_base_data_answers.loc[pd_base_data_answers['ID'] == id]
    source_series = subset.iloc[0]
    if len(subset) > 0:
        source_answer = source_series[answer_type]
        source_answer_len = len(nlp(source_answer)) if not pandas.isnull(source_answer) else 0
    else:
        source_answer = ""
        source_answer_len = None

    if not pandas.isnull(source_answer) and not pandas.isnull(generated_answer):
        # Calculate text length difference
        answer_len_difference_percent = 1/source_answer_len*generated_answer_len-1

        # Calculate cosine similarity
        cosine_similarity_embedding_percent = calc_cosine_similarity_embedding(nlp_processor(source_answer), nlp_processor(generated_answer))
        cosine_similarity_percent = cosine_distance_countvectorizer_method(nlp_processor(source_answer), nlp_processor(generated_answer))
    else:
        answer_len_difference_percent = None
        cosine_similarity_full_percent = None
        cosine_similarity_processed_percent = None

    output.append([
        series['AnswerType'],
        id,
        series['TryNbr'],
        series['Type'],
        series['StartDate'],
        series['ConsumedSecondsOverall'],
        generated_key,
        generated_key_len,
        source_answer,
        generated_answer,
        source_answer_len,
        generated_answer_len,
        answer_len_difference_percent,
        cosine_similarity_embedding_percent,
        cosine_similarity_percent
    ])
    print(id, series['TryNbr'])

actual_dt = datetime.today().strftime('%Y%m%d-%H%M-%S')
pandas.DataFrame(output, columns=evaluation_columns).to_excel(output_path+'EVAL_'+evaluation_filename+'_'+actual_dt+'.xlsx')