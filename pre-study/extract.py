import PyPDF2
import io
import pdfminer.

bookForGraduated = 'TextbookOfHumanAnatomy'
bookForDummies = 'AnatomyAndPhysiologyDummies'

textFile = io.open(bookForDummies+".txt", "w", encoding="utf-8")
reader = PyPDF2.PdfFileReader(bookForDummies+'.pdf')

pageNum = reader.getNumPages()

print(pageNum)

for page in range(0, 1):
    page = reader.getPage(page)
    pageText = page.extractText()
    print(page)
    textFile.write(pageText+"\r\n\r\n\r\n")

textFile.close()

