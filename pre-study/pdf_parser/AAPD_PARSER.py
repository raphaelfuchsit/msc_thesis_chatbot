import xml.etree.ElementTree as ET
import io

textFileName = 'AAPD_PARSED_CORRECT.txt'
textFile = io.open(textFileName, "w", encoding="utf-8")

tree = ET.parse('AAPD.xml')
root = tree.getroot()

fonts = []


def extract_text_textgroup(textgroup):
    for group_element in textgroup:
        if group_element.tag == 'textgroup':
            extract_text_textgroup(group_element)
        if group_element.tag == 'textbox':
            extract_text_box(group_element)


def extract_text_box(textbox):
    for line in textbox:
        extract_text_line(line)


def extract_text_line(line):
        lineFont = ""
        lineText = ""

        for text in line:
            if 'font' in text.attrib:
                lineFont = text.attrib['font']
                if text.attrib['font'] not in fonts:
                    fonts.append(text.attrib['font'])
            lineText += text.text

        if lineFont == 'TimesNewRoman,Bold':
            textFile.write("<|endoftext|>\r\n<|startoftext|>[TITLE]"+lineText.rstrip()+"\n[CONTENT]")
        elif lineText.find('Figure ') == -1:

            if '-\n' in lineText:
                formattedLine = lineText.replace('-\n', '')
            elif '\n' in lineText:
                formattedLine = lineText.replace('\n', ' ')
            else:
                formattedLine = lineText

            textFile.write(formattedLine)

for page in root:
    print(page.attrib['id'])


    for page_element in page:
        if page_element.tag == 'textgroup':
            extract_text_textgroup(page_element)
        if page_element.tag == 'textbox':
            extract_text_box(page_element)
        elif page_element.tag == 'line':
            extract_text_line(page_element)

    textFile.write(" ")

textFile.close()

print(fonts)
