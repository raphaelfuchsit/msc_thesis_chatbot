import xml.etree.ElementTree as ET
import io

textFileName = 'TOHA_PARSED_CONTROLS.txt'
textFile = io.open(textFileName, "w", encoding="utf-8")

parser = ET.XMLParser(encoding="utf-8")
tree = ET.parse('TOHA.xml')
root = tree.getroot()

fonts = []
sizes = []


def extract_text_textgroup(textgroup):
    for group_element in textgroup:
        if group_element.tag == 'textgroup':
            extract_text_textgroup(group_element)
        if group_element.tag == 'textbox':
            extract_text_box(group_element)


def extract_text_box(textbox):
    for line in textbox:
        extract_text_line(line)


def extract_text_line(line):
        lineText = ""
        lineFont = []
        lineSizeClass = []
        lineX=0
        lineY=0

        if 'bbox' in line.attrib:
            bboxArray = line.attrib['bbox'].split(",")
            lineX = float(bboxArray[2])
            lineY = float(bboxArray[3])

        for text in line:
            if 'font' in text.attrib:
                lineFont.append(text.attrib['font'])
                if text.attrib['font'] not in fonts:
                    fonts.append(text.attrib['font'])

            if 'size' in text.attrib:
                sizeFloat = float(text.attrib['size'])

                if 0 < sizeFloat < 10:
                    sizeClass = 'small'
                elif 10 <= sizeFloat < 12:
                    sizeClass = 'text'
                else:
                    sizeClass = 'title'

                lineSizeClass.append(sizeClass)
                if sizeClass not in sizes:
                    sizes.append(sizeClass)


            lineText += text.text


        if 'AFRQOI+Helvetica-Bold' in lineFont and 'title' in lineSizeClass and lineX < 800:
            textFile.write("<|endoftext|>\r\n<|startoftext|>[TITLE]"+lineText.rstrip()+"\n[CONTENT]")
        elif ('MCRTKE+Times-Roman' in lineFont or 'GZAPXX+Times-Italic' in lineFont or 'WZQFXJ+Times-BoldItalic' in lineFont) and ( 'text' in lineSizeClass):
            textFile.write(lineText.rstrip())

for page in root:
    print(page.attrib['id'])

    for page_element in page:
        if page_element.tag == 'textgroup':
            extract_text_textgroup(page_element)
        if page_element.tag == 'textbox':
            extract_text_box(page_element)
        elif page_element.tag == 'line':
            extract_text_line(page_element)

    textFile.write(" ")

textFile.close()

print(fonts)
print(sizes)