import pandas
import spacy
import re
from datetime import datetime
from collections import Counter
from scipy.spatial.distance import cosine

# nlp = spacy.load("de_core_news_sm")
nlp = spacy.load("de_core_news_lg")

pd_raw_data_answers = pandas.read_excel('X:\ZHAW\MscThesis\data\Wissenskorpus.xlsx', sheet_name='Antworten')


# def get_cosine_sim(*strs):
#     vectors = [t for t in get_vectors(*strs)]
#     return cosine_similarity(vectors)
#
# def get_vectors(*strs):
#     text = [t for t in strs]
#     vectorizer = CountVectorizer(text)
#     vectorizer.fit(text)
#     return vectorizer.transform(text).toarray()

output = []

for index, series in pd_raw_data_answers.iterrows():
    key = series['Textkey']

    if not pandas.isnull(series['Antwort']) and not pandas.isnull(series['Antwort2']):
        rawAnswer = series['Antwort']
        rawAnswer2 = series['Antwort2']

        sentences = [rawAnswer, rawAnswer2]
        vectors = [nlp(sentence).vector for sentence in sentences]

        cosine_similarity = cosine(vectors[0], vectors[1])
        cosine_similarity_percent = 1-cosine_similarity
        answer_len = len(nlp(series['Antwort']))
        answer2_len = len(nlp(series['Antwort2']))
        answer_len_difference_percent = 1/answer_len*answer2_len-1

        output.append([
            series['ID'],
            series['Textkey'],
            series['Antwort'],
            answer_len,
            series['Antwort2'],
            answer2_len,
            answer_len_difference_percent,
            cosine_similarity_percent
        ])

print(output)
actual_dt = datetime.today().strftime('%Y%m%d-%H%M-%S')
pandas.DataFrame(output).to_excel('similarity_'+actual_dt+'.xlsx')