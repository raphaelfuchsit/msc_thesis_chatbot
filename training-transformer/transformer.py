# MSc Thesis - Raphael Fuchs
# check https://devopedia.org/natural-language-processing
# PANDAS: https://pandas.pydata.org/pandas-docs/stable/index.html
# SPACY API: https://spacy.io/api/token, https://spacy.io/usage/spacy-101
# SPACY stop word removement: https://www.analyticsvidhya.com/blog/2019/08/how-to-remove-stopwords-text-normalization-nltk-spacy-gensim-python/
# Stop word removement 2: https://stackabuse.com/removing-stop-words-from-strings-in-python/
# SPACY Tokenization, Stemming, and Lemmatization: https://stackabuse.com/python-for-nlp-tokenization-stemming-and-lemmatization-with-spacy-library/
# Spacy spaCy Cheat Sheet: https://www.datacamp.com/community/blog/spacy-cheatsheet
# NLP TECHNIQUES: https://cognitechx.com/natural-language-processing-nlp-in-2020/
# NLP TECHNIQUES 2: https://devopedia.org/natural-language-processing

import pandas
import spacy
import re
from datetime import datetime

# nlp = spacy.load("de_core_news_sm")
nlp = spacy.load("de_core_news_lg")

content_detection_columns = ['key', 'raw', 'nlp_corrected']
content_detection_data_processed = []
content_detection_data_raw = []
content_generation_data_t1 = []
content_generation_data_t1_e2 = []
content_generation_data_t2 = []
content_generation_data_t1_t2_dk = []
content_generation_data_t1_t2_merged = []

# Generation constants
control_start = '<|startoftext|>'
control_end = '<|endoftext|>'
control_question = '[QUESTION]'
control_key = '[KEY]'
control_answer = '[ANSWER]'
control_answer2 = '[ANSWER2]'
should_new_line = 0

# to change
do_generate = True
do_generate_e2 = True
knowledge_base_url = ''


def check_spacy():
    sentence = "Was bedeutet Nephrologie?"
    doc = nlp(sentence)
    out = [[token.text, token.lemma_, token.pos_, token.tag_, token.dep_, token.shape_, token.is_alpha, token.is_stop] for token in doc]
    fprint(out)
    print(nlp_processor(doc))

def fprint(list):
    print(pandas.DataFrame(list).to_string())

def nlp_processor_2(raw_text):
    doc = nlp(raw_text)
    return ' '.join([word.lemma_.lower() for word in doc if (not word.is_stop or word.pos_ in  ['VERB','NOUN'] or word.tag_ in ['VVPP']) and not word.is_punct])

def nlp_processor(raw_text):
    doc = nlp(raw_text)
    return ' '.join([word.lemma_.lower() for word in doc if not word.is_stop and not word.is_punct])

def text_cleaning(raw_text):
    d1 = re.sub(':\n- ', ': ', raw_text)
    d2 = re.sub('\.\n- ', '. ', d1)
    d3 = re.sub('\n- ', ', ', d2)
    d4 = re.sub('\n', ' ', d3)
    d5 = re.sub(' +', ' ', d4)
    return d5

def export_training_data_detection(date_string, doc_name_suffix, data):
    file = open(base_url+"training_data/"+date_string+"_"+doc_name_suffix+".txt", "a", encoding="utf-8")
    for entry in data:
        string = "{}{}{}{}{}{}".format(control_start, control_question, entry[1], control_key, entry[0], control_end)
        file.write(string + ('\n' if should_new_line == 1 else ''))
    file.close()

def export_training_data_generation(date_string, doc_name_suffix, data):
    file = open(base_url+"training_data/"+date_string+"_"+doc_name_suffix+".txt", "a", encoding="utf-8")
    for entry in data:
        string = "{}{}{}{}{}{}".format(control_start, control_key, entry[0], control_answer, entry[1], control_end)
        file.write(string + ('\n' if should_new_line == 1 else ''))
    file.close()


# GENERATE TRAINING DATA FOR CONTENT DETECTION
def generate():
    print('1/3: generate data from excel')
    for index, series in pd_raw_data_answers.iterrows():
        key = series['Textkey']

        if not pandas.isnull(series['Antwort']):
            raw_text = series['Antwort']
            antwort = text_cleaning(raw_text)

            # SPLIT/PROCESS ANSWER INTO SENTENCES FOR DETECTION
            # TODO: (ENABLE/DISABLE): answer_to_question(doc)

            # SPLIT/PROCESS QUESTION FOR DETECTION
            pd_question = pd_raw_data_questions.loc[pd_raw_data_questions['Textkey'] == key]
            for q_index, q_series in pd_question.iterrows():
                content_detection_data_processed.append([key, nlp_processor(q_series['Frage'])])
                content_detection_data_raw.append([key, q_series['Frage']])

            # GENERATE TRAINING DATA FOR CONTENT GENERATION FOR EACH ANSWER
            content_generation_data_t1.append([key, antwort])

            # GENERATE TRAINING DATA FOR EXPERIMENT 2
            if do_generate_e2:
                if not pandas.isnull(series['Antwort2']) and series['IstE2'] == 1:
                    antwort2 = text_cleaning(series['Antwort2'])
                    # GENERATE
                    content_generation_data_t1_e2.append([key, antwort])
                    content_generation_data_t2.append([key, antwort2])
                    content_generation_data_t1_t2_dk.append([key+", Standard", antwort])
                    content_generation_data_t1_t2_dk.append([key+", Umformuliert", antwort2])
                    content_generation_data_t1_t2_merged.append([key, antwort, antwort2])


    if do_generate:
        # GENERATE TRAINING DATA OUTPUT FILE
        print('2/3: generate output files')
        actual_dt = datetime.today().strftime('%Y%m%d-%H%M-%S')

        # RAW EXCEL OUTPUT
        cd_out = open(base_url+"training_data/"+actual_dt+"_"+"csv_detection".upper()+".csv", "a")
        for entry in content_detection_data_processed:
            out_string = "{};{}".format(entry[1], entry[0])
            cd_out.write(out_string +'\n')
        cd_out.close()

        # TRAINING OUTPUT
        export_training_data_detection(actual_dt, 'detection_processed'.upper(), content_detection_data_processed)
        export_training_data_detection(actual_dt, 'detection_raw'.upper(), content_detection_data_raw)
        export_training_data_generation(actual_dt, 'generation_t1'.upper(), content_generation_data_t1)

        if do_generate_e2:
            export_training_data_generation(actual_dt, 'generation_t1_e2'.upper(), content_generation_data_t1_e2)
            export_training_data_generation(actual_dt, 'generation_t2'.upper(), content_generation_data_t2)
            export_training_data_generation(actual_dt, 'generation_t1_t2_different_key'.upper(), content_generation_data_t1_t2_dk)


            file = open(base_url+"training_data/"+actual_dt+"_"+"generation_t1_t2_merged".upper()+".txt", "a", encoding="utf-8")
            for entry in content_generation_data_t1_t2_merged:
                string = "{}{}{}{}{}{}{}{}".format(control_start, control_key, entry[0], control_answer, entry[1], control_answer2, entry[2], control_end)
                file.write(string + ('\n' if should_new_line == 1 else ''))
            file.close()

        print('3/3: CREATION SUCCESSFUL! -> ', actual_dt)

if __name__ == "__main__":
    # TODO: check_spacy()

    # Define if output should be generated
    do_generate = True
    do_generate_e2 = False

    # Open knowledge base data
    base_url = 'X:/ZHAW/thesis/msc_thesis_chatbot/experiments/e4/'
    knowledge_base_url = base_url+'Wissenskorpus_E4.xlsx'
    pd_raw_data_answers = pandas.read_excel(knowledge_base_url, sheet_name='Antworten')
    pd_raw_data_questions = pandas.read_excel(knowledge_base_url, sheet_name='FAQ')

    # Generate output data
    generate()

